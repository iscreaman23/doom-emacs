;; -*- no-byte-compile: t; -*-
;;; ui/treemacs/packages.el

(package! treemacs :pin "2bb14ac262")
(when (featurep! :editor evil +everywhere)
  (package! treemacs-evil
    :recipe (:host gitlab :repo "iscreaman23/treemacs"
            :files ("src/extra/treemacs-evil.el"))))
(package! treemacs-projectile)
(when (featurep! :tools magit)
  (package! treemacs-magit))
;;  (package! treemacs-magit
;;    :recipe (:host gitlab :repo "iscreaman23/magit"
;;            :files ("src/extra/treemacs-magit.el"))))
(when (featurep! :ui workspaces)
  (package! treemacs-persp
    :recipe (:host gitlab :repo "iscreaman23/treemacs"
            :files ("src/extra/treemacs-persp.el"))))
