(defun markdown-html (buffer)
  (princ (with-current-buffer buffer
    (format "<!DOCTYPE html><html><title>Impatient Markdown</title><xmp theme=\"united\" style=\"display:none;\"> %s  </xmp><script src=\"http://strapdownjs.com/v/0.2/strapdown.js\"></script></html>" (buffer-substring-no-properties (point-min) (point-max))))
  (current-buffer)))

(require 'org-inlinetask)

(setq org-directory "~/docs/org/")

(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

(bind-key* (kbd "C-;") 'counsel-M-x)

(customize-set-variable 'which-key-popup-type 'minibuffer)
(customize-set-variable 'max-mini-window-height 0.5)

(setq doom-theme 'doom-tomorrow-night)

(setq doom-font (font-spec :family "monospace" :size 18))

(defun set-frame-alpha (arg &optional active)
  (interactive "nEnter alpha value (1-100): \np")
  (let* ((elt (assoc 'alpha default-frame-alist))
         (old (frame-parameter nil 'alpha))
         (new (cond ((atom old)     `(,arg ,arg))
                    ((eql 1 active) `(,arg ,(cadr old)))
                    (t              `(,(car old) ,arg)))))
    (if elt (setcdr elt new) (push `(alpha ,@new) default-frame-alist))
    (set-frame-parameter nil 'alpha new)))
(global-set-key (kbd "C-c t") 'set-frame-alpha)

(setq display-line-numbers-type 'visual)
