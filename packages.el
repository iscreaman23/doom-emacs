;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

(package! bind-key)
(package! exwm)
(package! flymake)
(package! fzf)
(package! ox-pandoc)
(package! pandoc)
(package! rust-mode)
(package! rustic)
(package! shell-here)
(package! vimgolf)
(package! dash)
(package! epl)
(package! f)
(package! flycheck)
(package! ht)
(package! pkg-info)
(package! projectile)
(package! s)
(package! spinner)
(package! xelb)
(package! xterm-color)
(package! vterm)
(package! rainbow-mode)
(package! smooth-scrolling)
(package! undo-tree)
(package! projectile-ripgrep) ;; needs configuring
(package! ace-jump-mode)      ;; needs configuring
(package! lsp-mode)           ;; needs configuring
(package! impatient-mode)
(package! simple-httpd)
(package! htmlize)
(package! deft)
(package! org-roam)
(package! helpful)
(package! ranger)
(package! emms
  :recipe (:host github :repo "emacsmirror/emms"))
(package! evil
  :recipe (:host gitlab :repo "iscreaman23/evil"))
(package! exwm
  :recipe (:host gitlab :repo "iscreaman23/exwm"))
(package! evil-org
  :recipe (:host gitlab :repo "iscreaman23/evil-org-mode"))
(package! treemacs
  :recipe (:host gitlab :repo "iscreaman23/treemacs"))
(package! doom-themes
  :recipe (:host gitlab :repo "iscreaman23/emacs-doom-themes"))
